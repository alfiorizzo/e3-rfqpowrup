enum SMS {
  INIT = 0,
  STANDBY = 1,
  RESET = 2,
  NOMINAL =3,
  RAMPING =4,
  NOMINALSTARTUP =5
};



enum FIM {
  IDLE = 0,
  ARM = 1,
  HVON = 2,
  RFON = 3,
  ABO = 4
};







deque_type * TimesFastIlck;
deque_type * DetuningBuffer;
epicsTimeStamp IlckTimeNow;
epicsTimeStamp IlckTimeStart;
epicsTimeStamp IlckTimeStop;
epicsTimeStamp RampTimeStart;
epicsTimeStamp RampTimeNow;


/* Print Timestamp in Stdout */


static void printTSMsg(const char * fmt, ...) {
    char buffer[4096];
    va_list args;
    va_start(args, fmt);
    vsprintf(buffer, fmt, args);
    va_end(args);
    epicsTimeStamp now_ts;
    epicsTimeGetCurrent( &now_ts);
    char nowText[40];
    nowText[0] = 0;
    epicsTimeToStrftime(nowText,sizeof(nowText),"%Y/%m/%d %H:%M:%S.%03f",&now_ts);
    printf("%s:: %s", nowText,buffer);
}


