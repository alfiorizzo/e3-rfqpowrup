/*
SNL Code To Handle the Start Up of the RFQ from HVON to RFON and Auto-Reset
Author: Alfio Rizzo (C) ESS
mail: alfio.rizzo@ess.eu
~~~~~~~~~~~~~~~~~~~~~~~~~~

# Stages Code:
#       0 -> INIT
#       1 -> STANDBY
#       2 -> RESET
#       3 -> NOMINAL
#       4 -> RAMPING
#       5 -> NOMINALSTARTUP
*/


program rfqpowrup


/*
# Libraries
*/

%%#include <stdio.h>
%%#include <stdlib.h>
%%#include <string.h>
%%#include <unistd.h>
%%#include <stdarg.h>
%%#include <math.h>
%%#include <assert.h>
%%#include "deque.h"
%%#include "rfqpowrup.h"

#define DIG_MSG 40

/*
# Variables and Constants
*/

/* State Machine PVs*/
int SMStage;
assign SMStage to "{P}{R}SMStage";
monitor SMStage;

double SMEVRCycleFreq;
assign SMEVRCycleFreq to "{P}{R}EVR-CycleFreq-S";
monitor SMEVRCycleFreq;

double SMEVRSyncWdt;
assign SMEVRSyncWdt to "{P}{R}EVR-RFSyncWdt-S";
monitor SMEVRSyncWdt;

double SMLLRFPower;
assign SMLLRFPower to "{P}{R}LLRF-FFPulseGenP";
monitor SMLLRFPower;

char SMInfo[256];
assign SMInfo to "{P}{R}StageInfo";
monitor SMInfo;

int CheckForIlck;
assign CheckForIlck to "{P}{R}#CheckForIlck";
monitor CheckForIlck;

int IlckFlag;
assign IlckFlag to "{P}{R}#IlckFlag";
monitor IlckFlag;


int RampEnd;
assign RampEnd to "{P}{R}#RampEnd";
monitor RampEnd;

double TimeToReset;
assign TimeToReset to "{P}{R}TimeToReset";
monitor TimeToReset;

double RepRate[256];
assign RepRate to "{P}{R}RepRate-S";
monitor RepRate;

int RepRateSize;
assign RepRateSize to "{P}{R}RepRate-S.NORD";
monitor RepRateSize;

double PulseLength[256];
assign PulseLength to "{P}{R}PulseLength-S";
monitor PulseLength;

int PulseLengthSize;
assign PulseLengthSize to "{P}{R}PulseLength-S.NORD";
monitor PulseLengthSize;

double Power[256];
assign Power to "{P}{R}Power-S";
monitor Power;

int PowerSize;
assign PowerSize to "{P}{R}Power-S.NORD";
monitor PowerSize;

double RampWaitTime[256];
assign RampWaitTime to "{P}{R}RampWaitTime-S";
monitor RampWaitTime;

int RampWaitTimeSize;
assign RampWaitTimeSize to "{P}{R}RampWaitTime-S.NORD";
monitor RampWaitTimeSize;

double DetuningTolerance;
assign DetuningTolerance to "{P}{R}DetuningTolerance";
monitor DetuningTolerance;

double DetuningTime;
assign DetuningTime to "{P}{R}DetuningTime";
monitor DetuningTime;

int CloseLoop;
assign CloseLoop to "{P}{R}CloseLoop";
monitor CloseLoop;

int CheckDetuningTolerance;
assign CheckDetuningTolerance to "{P}{R}#CheckDetuningTolerance";
monitor CheckDetuningTolerance;


/* Input PVs*/

int FimState;
assign FimState to "{PP}FIMSTATE";
monitor FimState;

int RFPrecond;
assign RFPrecond to "{PP}RFPRECOND";
monitor RFPrecond;

int RFEnableMissSuperCondRB;
assign RFEnableMissSuperCondRB to "{PP}RFRFENMISUCNDRB";
monitor RFEnableMissSuperCondRB;

int RFCpuState;
assign RFCpuState to "{PP}RFCPUSTATE";
monitor RFCpuState;

double DetunePhaShftMovAvg;
assign DetunePhaShftMovAvg to "{PP}DETUNEPHSHMOVAVG";
monitor DetunePhaShftMovAvg;

int Dig1OpenLoop;
assign Dig1OpenLoop to "{PP}DIG1OPENLOOP";
monitor Dig1OpenLoop;

int Dig1RFCtrlCnstFFEn;
assign Dig1RFCtrlCnstFFEn to "{PP}DIG1RFCTRLCNSTFFEN";
monitor Dig1RFCtrlCnstFFEn;

int LLRFFreqTrackEn;
assign LLRFFreqTrackEn to "{PP}LLRFFREQTRACKEN";
monitor LLRFFreqTrackEn;

char Dig1Msg[DIG_MSG];
assign Dig1Msg to "{PP}DIG1MSG";
monitor Dig1Msg;

char Dig2Msg[DIG_MSG];
assign Dig2Msg to "{PP}DIG2MSG";
monitor Dig2Msg;

char Dig3Msg[DIG_MSG];
assign Dig3Msg to "{PP}DIG3MSG";
monitor Dig3Msg;

char Dig4Msg[DIG_MSG];
assign Dig4Msg to "{PP}DIG4MSG";
monitor Dig4Msg;

double RFSCycleFreq;
assign RFSCycleFreq to "{PP}RFSCYCLEFREQ";
monitor RFSCycleFreq;

double RFSSyncWdt;
assign RFSSyncWdt to "{PP}RFSSYNCWDT";
monitor RFSSyncWdt;

double LLRFPower;
assign LLRFPower to "{PP}LLRFPOWER";
monitor LLRFPower;

int RFSCPURst;
assign RFSCPURst to "{PP}RFSCPURST";
monitor RFSCPURst;

int RFSCPURFOn;
assign RFSCPURFOn to "{PP}RFSCPURFON";
monitor RFSCPURFOn;

int RFSFIMRst;
assign RFSFIMRst to "{PP}RFSFIMRST";
monitor RFSFIMRst;

int RFSFIMFSM;
assign RFSFIMFSM to "{PP}RFSFIMFSM";
monitor RFSFIMFSM;

int SkidLoopMode;
assign SkidLoopMode to "{PPS}SKIDLOOPMODE"; 
monitor SkidLoopMode;

int DetuneEnable;
assign DetuneEnable to "{PP}PHDETUNEEN";
monitor DetuneEnable;

/* Local Variables*/
int _CurrFimState, i, j, k, l, _Check, _IlckTimeWaitMsg, _RampStartFlag;
double _TimeSinceIlck, _TimeToRst, _Power, _RampCurrTime, _SetRepRate, _SetPulseLength, _SetPower;



/* Local Constants */
double _IlckDeadTime = 2.0;
double _TimeIlckLimit = 111.1;


/* Event flag */
evflag FimStateFlag;
sync FimState FimStateFlag; 

evflag DetuneFlag;
sync DetunePhaShftMovAvg DetuneFlag;



/*
# State Machine Definition
*/

ss rfqpowrup_monitor {
  state init {
    entry {
      strcpy(SMInfo,"[SM] Init Monitor Interlock\n");
      printTSMsg(SMInfo);
      pvPut(SMInfo);
      if (!TimesFastIlck)
        TimesFastIlck = deque_alloc();
      if (!DetuningBuffer)
        DetuningBuffer = deque_alloc();
    }

    when (pvConnectCount()==pvAssignCount()) {
        //printTSMsg("[SM2] All PVs connected\n");
    } state monitorilck
  
  }

  state monitorilck {
    entry{
      _CurrFimState = FimState;
      printTSMsg("[SM] Start Monitor Iterlock\n");
      l=0;
    }
    when (pvConnectCount()!=pvAssignCount()) {
      //printTSMsg("[SM2] Lost connection with one or more PVs\n");
      printTSMsg("[SM] Go back to Monitor Interlock INIT stage\n");
    } state init

    when (efTestAndClear(FimStateFlag) ) {
      if (FimState == HVON && _CurrFimState == RFON) {
        IlckTimeStart = pvTimeStamp(FimState);
        IlckFlag = TRUE;
        pvPut(IlckFlag);
      }
      if (FimState == RFON && _CurrFimState == HVON && IlckFlag){  
        IlckTimeStop = pvTimeStamp(FimState);
        _TimeSinceIlck = fabs(epicsTimeDiffInSeconds(&IlckTimeStop,&IlckTimeStart));
        if (_TimeSinceIlck < _TimeIlckLimit){
          deque_push_back(TimesFastIlck, IlckTimeStart.secPastEpoch + IlckTimeStart.nsec*1e-9);
        }
        IlckFlag = FALSE;
        pvPut(IlckFlag);  
      }
      _CurrFimState = FimState;
    } state monitorilck 

    when(SMStage == NOMINAL || SMStage == RAMPING || SMStage == NOMINALSTARTUP){
      if (_CurrFimState == HVON && IlckFlag) {
          epicsTimeGetCurrent( &IlckTimeNow);
          _TimeToRst = _IlckDeadTime - fabs(epicsTimeDiffInSeconds(&IlckTimeNow,&IlckTimeStart));
          if ( l % 10000 ==0){
            if (_TimeToRst < 0)
              _TimeToRst = 0;
            printTSMsg("[SM] Time to reset %f\n",_TimeToRst);
            TimeToReset = _TimeToRst;
            pvPut(TimeToReset);
          }
          if (fabs(epicsTimeDiffInSeconds(&IlckTimeNow,&IlckTimeStart)) > _IlckDeadTime) {
            CheckForIlck=TRUE;
            pvPut(CheckForIlck);
            l = 0;  
          }
          else {
            CheckForIlck=FALSE;
            pvPut(CheckForIlck);
            l +=1;
          }      
      }
      else {
        CheckForIlck=FALSE;
        pvPut(CheckForIlck);
        l = 0;
      } 
    } state monitorilck

    when(efTestAndClear(DetuneFlag)){
      deque_push_back(DetuningBuffer,DetunePhaShftMovAvg);
    } state monitorilck

  }

}


ss rfqpowrup_statemachine {
    
    /* Start init*/
    state init{
      entry{
        printTSMsg("[SM] Preliminary controls executed: Initialization\n");
        printTSMsg("[SM] Checking connection to PVs......\n");
        SMStage = INIT;
        pvPut(SMStage);
      } 

      when (pvConnectCount()==pvAssignCount()) {
        printTSMsg("[SM] All PVs connected\n");
      } state standby
         
        
    } /*End init*/
    
    /* Start standby*/
    state standby {
      entry{
        printTSMsg("[SM] Enter in STANDBY stage\n");
        printTSMsg("[SM] Waiting for Operator input\n");
        SMStage = STANDBY;
        pvPut(SMStage, SYNC);
      }
      
      when (pvConnectCount()!=pvAssignCount()) {
        printTSMsg("[SM] Lost connection with one or more PVs\n");
        printTSMsg("[SM] Go back to STATE MACHINE INIT stage\n");
      } state init

      when (SMStage == NOMINAL) {
        printTSMsg("[SM] Go to NOMINAL stage by Operator\n");
      } state nominal

      when (SMStage == RESET){
        printTSMsg("[SM] Go to RESET stage by Operator\n");
      } state reset

      when (SMStage == RAMPING || SMStage == NOMINALSTARTUP || SMStage == INIT){
        printTSMsg("[SM] Not allowed Transition from STANDBY by Operator!!\n");
        SMStage = STANDBY;
        pvPut(SMStage, SYNC); 
      } state standby

    } /*End standby*/
    
    /* Start nominal*/
    state nominal {
      entry {
        printTSMsg("[SM] Enter in NOMINAL stage\n");
        SMStage = NOMINAL ;
        pvPut(SMStage, SYNC); 
      }

      when (SMStage == STANDBY) {
        printTSMsg("[SM] Go to STANDBY stage by Operator\n");
      } state standby

      when (pvConnectCount()!=pvAssignCount()) {
        printTSMsg("[SM] Lost connection with one or more PVs\n");
        printTSMsg("[SM] Go back to STATE MACHINE INIT stage\n");
      } state init

      when(CheckForIlck) {
        SMStage = RESET;
        pvPut(SMStage);
        strcpy(SMInfo,"[SM] Ilck: Go to RESET Stage\n"); 
        printTSMsg(SMInfo);
        pvPut(SMInfo);
      } state reset 

      when (SMStage == RAMPING || SMStage == NOMINALSTARTUP || SMStage == INIT || SMStage == RESET){
        printTSMsg("[SM] Not allowed Transition from NOMINAL by Operator!!\n");
        SMStage = NOMINAL;
        pvPut(SMStage); 
      } state nominal

      


    } /*End nominal*/
    
    
    /*Start reset*/
    
    state reset {
      entry {
        printTSMsg("[SM] Enter in Reset stage\n");
        SMStage = RESET ;
        pvPut(SMStage, SYNC);
        strcpy(SMInfo,"[SM] !!----------- Full Reset -------------!!\n");
        printTSMsg(SMInfo);
        pvPut(SMInfo);
        printTSMsg("[SM] Open LLRF Loop\n");
        Dig1OpenLoop = 1;
        pvPut(Dig1OpenLoop, SYNC);
        Dig1RFCtrlCnstFFEn = 0;
        pvPut(Dig1RFCtrlCnstFFEn, SYNC);
        printTSMsg("[SM] Swtich LLRF tracking on\n");
        LLRFFreqTrackEn = 1;
        pvPut(LLRFFreqTrackEn, SYNC);
        
        // Stop LLRF
        printTSMsg("[SM] Rearm LLRF\n");
        strcpy(Dig1Msg,"RESET");
        pvPut(Dig1Msg, SYNC);
        strcpy(Dig2Msg, "RESET");
        pvPut(Dig2Msg, SYNC);
        strcpy(Dig3Msg, "RESET");
        pvPut(Dig3Msg, SYNC);
        strcpy(Dig4Msg, "RESET");
        pvPut(Dig4Msg, SYNC); 
        sleep(0.01);
        strcpy(Dig1Msg,"INIT");
        pvPut(Dig1Msg, SYNC);
        strcpy(Dig2Msg, "INIT");
        pvPut(Dig2Msg, SYNC);
        strcpy(Dig3Msg, "INIT");
        pvPut(Dig3Msg, SYNC);
        strcpy(Dig4Msg, "INIT");
        pvPut(Dig4Msg, SYNC); 

        // Put startup settings
        printTSMsg("[SM] Startup Settings: %f Hz, %f us, %f kW\n", RepRate[0], PulseLength[0], Power[0]);
        RFSCycleFreq = RepRate[0];
        pvPut(RFSCycleFreq,SYNC);
        RFSSyncWdt = PulseLength[0];
        pvPut(RFSSyncWdt, SYNC);
        LLRFPower = Power[0];
        pvPut(LLRFPower, SYNC);

        // Reset SIM & FIM
        printTSMsg("[SM] Reset SIM\n");
        RFSCPURst = 1;
        pvPut(RFSCPURst, SYNC); //WHY TWO TIMES IN THE SCRIPT ? 
        RFSCPURFOn = 1;
        pvPut(RFSCPURFOn, SYNC);

        i = 0; //MAYBE NOT NEEDED

        
      }

      when (!(RFPrecond == 1 && RFEnableMissSuperCondRB == 1 &&  RFCpuState ==5 )) { //TO CHECK , MAYBE PUT IN THE CONDITION RFSFIMRst and RFSFIMFSM
        printTSMsg("[SM] Reset ILs %d",i);
        RFSFIMRst = 1;
        pvPut(RFSFIMRst, SYNC);
        printTSMsg("Reset FIM %d",i);
        RFSFIMFSM = RFON;
        pvPut(RFSFIMFSM, SYNC); 
      } state reset

      

      when (RFPrecond == 1 && RFEnableMissSuperCondRB == 1 &&  RFCpuState ==5  && strcmp(Dig1Msg, "ON") !=0  && strcmp(Dig2Msg, "ON") !=0 && strcmp(Dig3Msg, "ON") !=0 && strcmp(Dig4Msg,"ON") !=0){
        printTSMsg("[SM] Restart LLRF\n");
        strcpy(Dig1Msg,"ON");
        pvPut(Dig1Msg, SYNC);
        strcpy(Dig2Msg, "ON");
        pvPut(Dig2Msg, SYNC);
        strcpy(Dig3Msg, "ON");
        pvPut(Dig3Msg, SYNC);
        strcpy(Dig4Msg, "ON");
        pvPut(Dig4Msg, SYNC); 
      } state reset


      when (!IlckFlag && RFPrecond == 1 && RFEnableMissSuperCondRB == 1 &&  RFCpuState ==5  && strcmp(Dig1Msg, "ON")==0 && strcmp(Dig2Msg,"ON")==0 && strcmp(Dig3Msg,"ON")==0 && strcmp(Dig4Msg,"ON")==0){
        printTSMsg("[SM] Go To Ramping Stage\n");
        SMStage = RAMPING;
        pvPut(SMStage);
      } state ramping

      when (pvConnectCount()!=pvAssignCount()) {
        printTSMsg("[SM] Lost connection with one or more PVs\n");
        printTSMsg("[SM] Go back to INIT stage\n");
      } state init

      when (SMStage == STANDBY) {
        printTSMsg("[SM] Go to STANDBY stage by Operator\n");
      } state standby

      when (SMStage == NOMINALSTARTUP || SMStage == INIT || SMStage == NOMINAL){
        printTSMsg("[SM] Not allowed Transition from RESET by Operator!!\n");
        SMStage = RESET;
        pvPut(SMStage); 
      } state reset

    }  /*End reset*/
    
    /* Start Ramping */
    state ramping{
      entry {
        printTSMsg("[SM] Enter in Ramping stage\n");
        SMStage = RAMPING ;
        pvPut(SMStage);
        RampEnd = FALSE;
        pvPut(RampEnd);
        _IlckTimeWaitMsg = FALSE;
        _RampStartFlag = FALSE;
        k = 0;
      }

      
      when(CheckForIlck) {
        SMStage = RESET;
        pvPut(SMStage);
        strcpy(SMInfo,"[SM] Ilck: Go to RESET Stage\n"); 
        printTSMsg(SMInfo);
        pvPut(SMInfo);
      } state reset 
      

      when(!CheckForIlck && !RampEnd){
        epicsTimeGetCurrent(&RampTimeNow);
        _RampCurrTime = RampTimeNow.secPastEpoch + RampTimeNow.nsec*1e-9;
        if (_RampStartFlag){
          for (j=0; j < deque_size(TimesFastIlck); j++){
            if ( (_RampCurrTime - deque_get_at(TimesFastIlck,j)) < RampWaitTime[k] ) {
              epicsTimeGetCurrent(&RampTimeStart);
              if (_IlckTimeWaitMsg)
                printTSMsg("[SM] Ilck in this setting, wait longer!\n");
              
              _IlckTimeWaitMsg= FALSE;
              break;
            } 
          }    
        }

        if ( ( (k ==0 )  || (epicsTimeDiffInSeconds(&RampTimeNow, &RampTimeStart) > RampWaitTime[k])) && ( k < RampWaitTimeSize)  ){
          _SetRepRate = fmin(RepRate[k],SMEVRCycleFreq);
          _SetPulseLength = fmin(PulseLength[k], SMEVRSyncWdt);
          _SetPower = fmin(Power[k], SMLLRFPower);

          printTSMsg("[SM] Ramp %f Hz, %f us, %f kW\n", _SetRepRate, _SetPulseLength, _SetPower);


          RFSCycleFreq = _SetRepRate;
          pvPut(RFSCycleFreq, SYNC);

          RFSSyncWdt = _SetPulseLength;
          pvPut(RFSSyncWdt, SYNC);

          LLRFPower =  _SetPower;
          pvPut(LLRFPower, SYNC);
          
          
          epicsTimeGetCurrent(&RampTimeStart);
          _RampStartFlag = TRUE;
          _IlckTimeWaitMsg = TRUE;
          k +=1;
              
        }

        if (k == RampWaitTimeSize || SMStage == STANDBY || SMStage == NOMINAL){
          RampEnd = TRUE;
          pvPut(RampEnd, SYNC);      
        }

        if (SMStage == INIT || SMStage == NOMINALSTARTUP || SMStage == RESET ){
          printTSMsg("[SM] Not allowed Transition from RAMPING by Operator!!\n");
          SMStage = RAMPING;
          pvPut(SMStage,SYNC);
        } 
      } state ramping

      when (!CheckForIlck && RampEnd){
        printTSMsg("[SM] Reached end of ramp: back to Nominal Startup stage\n");
        SkidLoopMode = 0;
        pvPut(SkidLoopMode, SYNC);
      } state nominalstartup

      when (pvConnectCount()!=pvAssignCount()) {
        printTSMsg("[SM] Lost connection with one or more PVs\n");
        printTSMsg("[SM] Go back to INIT stage\n");
      } state init

      
      

      //when (SMStage == INIT || SMStage == NOMINAL || SMStage == NOMINALSTARTUP || SMStage == RESET || SMStage == STANDBY){
      //  printTSMsg("[SM] Not allowed Transition from RAMPING by Operator!!\n");
      //  SMStage = RAMPING;
      //  pvPut(SMStage,SYNC); 
      //} state ramping

    } /*End Ramping*/

    /* Start Nominal Startup (waiting for frequency to be stable) */
    state nominalstartup{
      
      entry {
        printTSMsg("[SM] Enter in Nominal Startup Stage\n");
        SMStage = NOMINALSTARTUP ;
        pvPut(SMStage, SYNC);
        CheckDetuningTolerance = FALSE;
        pvPut(CheckDetuningTolerance, SYNC);
        _Check = FALSE;
      }    

      //CHECK THIS TRANSITION
      when(!CheckForIlck && !CheckDetuningTolerance){
        for (j=0; j<deque_size(DetuningBuffer); j++){
          if (fabs(deque_get_at(DetuningBuffer,j)) < DetuningTolerance  ){
            _Check = TRUE;    
          }
          else{
            _Check = FALSE;
            break;
          }
        }
        if (_Check){
          CheckDetuningTolerance = _Check;
          pvPut(CheckDetuningTolerance, SYNC);    
        }
      } state nominalstartup

      when(CheckDetuningTolerance){
        printTSMsg("[SM] Swtich Off LLRF Tracking\n");
        LLRFFreqTrackEn = 0;
        pvPut(LLRFFreqTrackEn, SYNC);
        
        //WHY THIS ??
        _Power = pvGet(LLRFPower,SYNC);
        LLRFPower = _Power + 0.01;
        pvPut(LLRFPower, SYNC);
        sleep(0.01);
        LLRFPower = _Power;
        pvPut(LLRFPower);
        if (CloseLoop){
          printTSMsg("[SM] Switch LLRF feedback on\n");
          Dig1OpenLoop = 0;
          pvPut(Dig1OpenLoop, SYNC);
        }
        
        printTSMsg("[SM] Go To Nominal\n");
      } state nominal

      when (SMStage == STANDBY) {
        printTSMsg("[SM] Go to STANDBY stage by Operator\n");
      } state standby
    
      when(CheckForIlck) {
        SMStage = RESET;
        pvPut(SMStage);
        strcpy(SMInfo,"[SM] Ilck: Go to RESET Stage\n"); 
        printTSMsg(SMInfo);
        pvPut(SMInfo);
      } state reset 

      when (pvConnectCount()!=pvAssignCount()) {
        printTSMsg("[SM] Lost connection with one or more PVs\n");
        printTSMsg("[SM] Go back to INIT stage\n");
      } state init
    
      when (SMStage == INIT || SMStage == NOMINAL || SMStage == RAMPING || SMStage == RESET){
        printTSMsg("[SM] Not allowed Transition from NOMINALSTARTUP by Operator!!\n");
        SMStage = NOMINALSTARTUP;
        pvPut(SMStage); 
      } state nominalstartup
    
    } /*End Nomninal Startup */



} /*End SM */
